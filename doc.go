// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

// This is the documentation of the test-otto
//
// Introduction
//
// test-otto is a simple package illustrating basic features of the otto package
// github.com/robertkrimen/otto. This package provides a javascript interpeter.
//
// test-otto defines several types and interfaces to be used for reading external library from a ".js" file
// They might be useful to be incorporated with github.com/google/cayley/ package, namelly to the function
//
//   func newWorker(qs graph.QuadStore) *worker {}
//
// in https://github.com/google/cayley/blob/master/query/gremlin/environ.go
//
// To view this godoc on your own machine, just run
//
//   go get code.google.com/p/go.tools/cmd/godoc
//   go get -u bitbucket.org/iggy_floyd/test-otto/  (this repo is made to be public, to allow such "go-getting" )
//   godoc -http=:8080
//
// Then you can open documentation in the browser:
//
//  http://localhost:8080/pkg/bitbucket.org/iggy_floyd/test-otto/
//
// You can clone the git repo
//
//     git clone https://iggy_floyd@bitbucket.org/iggy_floyd/test-otto.git
//
// and execute the external JS library
//
//  ./test-otto simple.js
//
// Also it is possible to run several libraries in once
//
//    ls simple*js > libraries.txt 
//    ./test-otto --lib  libraries.txt
//
//
package main
