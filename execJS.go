// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./jsloader"
	"bufio"

	"github.com/robertkrimen/otto"
	"io"
	"os"
	"time"
)

// executes content of a single JS library
func execJS(loader jsloader.ILoader, vm *otto.Otto) (*otto.Otto, error) {

	src, err := loader.ReadSource()
	if err != nil {
		return nil, err
	}
	_vm := vm
	if _vm == nil {
		_vm = otto.New()
	}
	_, err = _vm.Run(src)
	return _vm, err
}

// reads a text file and executes all found js files there
// returns the js virtual machine initialized by the executed js files
func loadJSfromFile(fileName string) (*otto.Otto, error) {

	f, err := os.Open(fileName)

	if err != nil {
		defer f.Close()
		return nil, &jsloader.Error{time.Now(), err.Error()}
	}

	r := bufio.NewReader(f)
	line, err := r.ReadString('\n')

	vm := otto.New()
	var loader jsloader.Loader
	for err == nil {
		line = line[:len(line)-1]

		loader = jsloader.Loader{line}
		vm, err = execJS(loader, vm)
		if err != nil {
			return nil, err
		}

		line, err = r.ReadString('\n')
	}
	if err == io.EOF {
		return vm, nil
	}

	return nil, err

}
