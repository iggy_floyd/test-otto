// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package main

import (
	"./jsloader"
	"flag"
	"fmt"
	"github.com/robertkrimen/otto"
	"os"
)

func main() {

	lib := flag.String("lib", "", "a file with a list of js libraries")
	flag.Parse()

	if *lib != "" {

		_, err := loadJSfromFile(*lib)
		if err != nil {
			switch err := err.(type) {
			case *otto.Error:
				fmt.Print(err.String())
			case *jsloader.Error:
				fmt.Println(err)
			default:
				fmt.Println(err)
			}
			os.Exit(64)
		}

	} else {

		loader := jsloader.Loader{flag.Arg(0)}
		_, err := execJS(loader, nil)

		if err != nil {
			switch err := err.(type) {
			case *otto.Error:
				fmt.Print(err.String())
			case *jsloader.Error:
				fmt.Println(err)
			default:
				fmt.Println(err)
			}
			os.Exit(64)
		}
	}

}
