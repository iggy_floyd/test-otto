# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project


GOROOT := /home/debian/debian-package/go/go
GOPATH := /home/debian/debian-package/go-modules



all: build 
	

ini: 
	export GOROOT=/home/debian/debian-package/go/go
	export PATH=$(PATH):$(GOROOT)/bin
	
build:
	@export GOROOT=$(GOROOT); find ./ -iname "*go"|  xargs -I {}  $(GOROOT)/bin/gofmt -s -w {}
	@export GOROOT=$(GOROOT); export GOPATH=$(GOPATH); find `pwd` -iname "main*go" |  xargs -I {} echo "cd \`dirname {}\`; $(GOROOT)/bin/go  build -o \`basename \\\`dirname {}\\\`\`" | bash

clean:
	@find . -type f -executable -print | xargs -I {} rm {}

readme:
	 @export GOROOT=$(GOROOT); export GOPATH=$(GOPATH); find `pwd` -iname "doc*go" |  xargs -I {} echo "cd \`dirname {}\`; $(GOPATH)/bin/godocdown  -output=README.md" | bash

.PHONY: build clean all ini readme
