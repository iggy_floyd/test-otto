// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
// // simple.js is a simple JS program 
// to be tested with otto executable
/*
 * 
 * ../../go-modules/src/github.com/robertkrimen/otto/otto/otto simple2.js 
 * 
 * 
 */


// performs multiplication
function mul(num1,num2) {
    
    return parseInt(num1) * parseInt(num2);
    
}

// prints the sum of two numbers
function printMul(num1,num2) {
    
    console.log("mutiplication is", mul(num1,num2));
}



//print multiplication of 1 and 2
printMul(1,2);
