// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package jsloader

import (
	"io/ioutil"
	"time"
)

// ILoader defines the interface of the jsloader
type ILoader interface {
	ReadSource() ([]byte, error)
}

// represents jsloader
type Loader struct {
	FileName string
}

// reads the js library and returns the slice of bytes
func (jsl Loader) ReadSource() ([]byte, error) {
	if jsl.FileName == "" {
		return nil, &Error{time.Now(), "empty file name"}

	}
	src, err := ioutil.ReadFile(jsl.FileName)
	if err != nil {
		return nil, &Error{time.Now(), err.Error()}
	}
	return src, err
}
