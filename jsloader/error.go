// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package jsloader

import (
	"fmt"
	"time"
)

// to handle errors of jsloader
type Error struct {
	When time.Time
	What string
}

// Error is a basic method of the interface error
func (e *Error) Error() string {
	return fmt.Sprintf("at %v, %s", e.When, e.What)
}
