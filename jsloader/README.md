# jsloader
--
    import "."

defines a few types and interfaces to work-out with otto engine

## Usage

#### type Error

```go
type Error struct {
	When time.Time
	What string
}
```

to handle errors of jsloader

#### func (*Error) Error

```go
func (e *Error) Error() string
```
Error is a basic method of the interface error

#### type ILoader

```go
type ILoader interface {
	ReadSource() ([]byte, error)
}
```

ILoader defines the interface of the jsloader

#### type Loader

```go
type Loader struct {
	FileName string
}
```

represents jsloader

#### func (Loader) ReadSource

```go
func (jsl Loader) ReadSource() ([]byte, error)
```
reads the js library and returns the slice of bytes
