package main

// how to get github.com/robertkrimen/otto to a local place?
/*

 go get github.com/robertkrimen/otto

*/

import (
	"./test"
	"fmt"
)

// test of the otto
func main() {

	vm := test.TestOtto()
	value, _ := vm.Get("abc")
	valueOut, _ := value.ToInteger()
	fmt.Println("Value obtainer from Otto: ", valueOut)

}
