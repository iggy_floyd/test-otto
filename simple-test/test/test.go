package test

import (
	"github.com/robertkrimen/otto"
)

func TestOtto() *otto.Otto {
	vm := otto.New()
	vm.Run(`
abc = 2 + 2;
console.log("The value of abc is " + abc); // 4
`)

	return vm
}
