// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
// // simple.js is a simple JS program 
// to be tested with otto executable
/*
 * 
 * ../../go-modules/src/github.com/robertkrimen/otto/otto/otto simple.js 
 * 
 * 
 */


// performs summutation
function sum(num1,num2) {
    
    return parseInt(num1) + parseInt(num2);
    
}

// prints the sum of two numbers
function printSum(num1,num2) {
    
    console.log("sum is", sum(num1,num2));
}



//print sum of 1 and 2
printSum(1,2);
